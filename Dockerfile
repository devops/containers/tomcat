ARG tomcat_version=7
ARG image_variant=jdk8-openjdk-buster
FROM tomcat:${tomcat_version}-${image_variant}
#
# See https://hub.docker.com/_/tomcat.
#
# CATALINA_BASE:   /usr/local/tomcat
# CATALINA_HOME:   /usr/local/tomcat
# CATALINA_TMPDIR: /usr/local/tomcat/temp
# JRE_HOME:        /usr
# CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar
#
# CATALINA_HOME/bin is in PATH.
#
ENV TOMCAT_USER  tomcat
ENV TOMCAT_UID   8080
ENV TOMCAT_GROUP tomcat
ENV TOMCAT_GID   8080

LABEL io.openshift.expose-services="8080:http"
LABEL io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN set -eux; \
	apt-get -y update; \
	apt-get -y upgrade; \
	apt-get -y install \
	  gosu \
	  less \
	  locales \
 	  locales-all \
	  wait-for-it \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

# Set locale and time zone
ENV LANG     en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL   en_US.UTF-8
ENV TZ       US/Eastern

RUN locale-gen $LANG

WORKDIR $CATALINA_HOME

COPY ./tomcat/ ./
COPY ./s2i/bin /usr/libexec/s2i

RUN mv ./webapps.dist/manager ./webapps/

HEALTHCHECK CMD healthcheck

CMD [ "/usr/libexec/s2i/usage" ]
